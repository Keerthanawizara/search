const propertySchema = require('./propertySchema');
const apiCaller = require('./propertyService');
const assesseeSchema = require('../Assessee/assesseeSchema');
const mongoose = require('mongoose');

//const propertyList=mongoose.model('propertyList', PropertySchema, 'propertyList');

const propertyDetail = async (req) => {
  let docs = await propertySchema.create(req)
  if (docs) {
    let apiRes = await apiCaller.getAllProperty()
    console.log(apiRes)
    return docs
  } else {
    return err
  }
}

const propertyDataList = async (req) => {
  let pageNo = req.payload.pageNo || 0;
  let pageSize = req.payload.pageSize || 10;
  console.log("pageNo", pageNo)
  const requestKeys = Object.keys(req.payload.search)[0]
  const requestValues = Object.values(req.payload.search)[0]
  let docs = [];
  if (Object.keys(req.payload.search).length === 0) {
    docs = await propertySchema.paginate({}, { offset: pageNo, limit: pageSize, sort: { _id: -1 } })
  } else {
    docs = await propertySchema.paginate({ [requestKeys]: { $regex: requestValues, $options: 'i' }, populate: "assesseeList" }, { offset: pageNo, limit: pageSize, sort: { _id: -1 } })
  }
  if (docs) {
    return (docs)
  }
}



const propertyRecord = async (req) => {
  let docs = await propertySchema.findOne(req)
  if (docs) {
    return docs;
  } else {
    return err;
  }
}


const propertyRecordUpdate = async (req, params) => {
  let docs = await propertySchema.updateOne(req, { $set: params })
  if (docs) {
    return docs;
  } else {
    return err;
  }
}


const propertyRecordDelete = async (req) => {
  let docs = await propertySchema.deleteOne(req)
  if (docs) {
    return docs;
  } else {
    return err;
  }
}

const propertyDuplicateChecker = async (req) => {
  let docs = await propertySchema.findOne({ propertyNumber: req });
  if (docs) {
    return docs;
  }
}

// const SearchRecord = async (req) => {
//   let pageNo = req.payload.pageNo || 0
//   let pageSize = req.payload.pageSize || 10
//   const requestKeys = Object.keys(req.payload.search)[0]
//   const requestValues = Object.values(req.payload.search)[0]
//    let assesseeRecords = await assesseeSchema.paginate({ [requestKeys]: { $regex: requestValues, $options: 'i' } }, { offset: pageNo, limit: pageSize, sort: { _id: -1 } });
//   let propertyRecords = await propertySchema.paginate({ [requestKeys]: { $regex: requestValues, $options: 'i' } }, { offset: pageNo, limit: pageSize, sort: { _id: -1 } });
  
//   let data={};
//   data.docs = [];
  
//   let assesseeDocs = [];
//   let propertyDocs = []
//   console.log("DDDDDDDDDDDDDDDDD",propertyRecords)
//   console.log("FFFFFFFFFFF",assesseeRecords)

  
//   await propertyRecords.docs.forEach((h) => {
//     let res = {};
//     res.address = h.address;
//     res.propertyNumber = h.propertyNumber;
//     propertyDocs.push(res)
//   })
// console.log("CCCCCCCCCCCCCCCCCCC",propertyRecords.docs)
//  await assesseeRecords.docs.forEach((e, i) => {
//     let doc = {}
//     doc.cellPhone = e.cellPhone;
//     doc.emailAddress = e.emailAddress;
//     doc.name = e.name;
//     doc.propertyNumber = e.propertyNumber;
//     doc.property_id = e.property_id;
//     assesseeDocs.push(doc);
//   });
 
//  await propertyDocs.forEach((e,i) => data.docs.push({...propertyDocs[i], ...assesseeDocs[i]}));
//   console.log("property",data)
//   return propertyRecords
// }


const SearchRecord = async(req)=>{
   let pageNo = req.payload.pageNo || 0
  let pageSize = req.payload.pageSize || 10
  const requestKeys = Object.keys(req.payload.search)[0]
  const requestValues = Object.values(req.payload.search)[0]
 propertySchema.aggregate(
   [
     {
        $group: {_id : { propertyNumber: { propertyNumber: "$9990033keerthi" }}}
     },
{$lookup:
       {
        from:"propertyList" ,
        localField:"propertyNumber" ,
        foreignField:"propertyNumber" ,
        as:  "property_List"
       }
 },
 {$lookup:
  {
   from:"assesseeList" ,
   localField:"propertyNumber" ,
   foreignField:"propertyNumber" ,
   as:  "assessee_List"
  }
},
 {
      $match:{[requestKeys]:{$regex:requestValues,$options:"i"}}
 },
 {
       $skip:pageNo
 },
 {
       $limit:pageSize
 },
 {
   $sort:{_id:-1}
 }   
  ],function(err,docs){
    if(docs){
      return docs
    }else{
      return err
    }
  })
}





module.exports = {
  propertyDetail,
  propertyDataList,
  propertyRecord,
  propertyRecordUpdate,
  propertyRecordDelete,
  propertyDuplicateChecker,
  SearchRecord
} 